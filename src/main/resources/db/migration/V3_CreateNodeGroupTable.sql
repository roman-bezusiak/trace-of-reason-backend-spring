CREATE TABLE IF NOT EXISTS node_group (
    id UUID PRIMARY KEY NOT NULL UNIQUE DEFAULT uuid_generate_v4(),
    name VARCHAR(256) NOT NULL,
    description TEXT NOT NULL UNIQUE,
    creation_timestamptz TIMESTAMPTZ NOT NULL,
	update_timestamptz TIMESTAMPTZ NOT NULL,
	aliases TEXT[],
    authority_ids UUID[] NOT NULL
);
