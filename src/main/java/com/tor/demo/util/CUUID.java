package com.tor.demo.util;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.lang.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Compact UUID
 *
 * Allows for converting UUID string representation without hyphens into UUID or UUID[].
 * */
public class CUUID {
    public static final int CUUID_STRING_LENGTH = 32;
    public char[] chars;

    public CUUID() {
        this.chars = new char[CUUID_STRING_LENGTH];
    }

    public CUUID(@JsonProperty("cuuid") char[] chars) {
        this.chars = chars;
    }

    public CUUID(@JsonProperty("cuuid") String cuuidString) {
        this.chars = cuuidString.toCharArray();
    }

    @Nullable
    public static UUID toUUID(@Nullable CUUID cuuid) {
        if (!CUUID.isValid(cuuid)) return null;

        char[] uuid = new char[CUUID_STRING_LENGTH + 4];
        uuid[8] = '-';
        uuid[13] = '-';
        uuid[18] = '-';
        uuid[23] = '-';

        for (int i = 0, j = 0; j < CUUID_STRING_LENGTH; ++i, ++j) {
            if (uuid[i] == '-') ++i;
            uuid[i] = cuuid.chars[j];
        }

        return UUID.fromString(String.valueOf(uuid));
    }

    /**
     * Converts UUID String representation into CUUID String representation
     * */
    @Nullable
    public static String UUIDStringToCUUIDString(@Nullable String uuid) {
        if (
            uuid == null ||
            uuid.length() != CUUID_STRING_LENGTH + 4 ||
            !uuid.matches("^[0-9a-fA-F\\-]*$")
        ) return null;

        char[] cuuid = new char[CUUID_STRING_LENGTH];
        char charBuffer;
        int uuidLength = uuid.length();

        for (int i = 0, j = 0; j < uuidLength; ++j) {
            charBuffer = uuid.charAt(j);
            if (charBuffer != '-') {
                cuuid[i] = charBuffer;
                ++i;
            }
        }

        return String.valueOf(cuuid);
    }

    public static boolean isValid(@Nullable CUUID cuuid) {
        if (
            cuuid == null ||
            cuuid.chars.length != CUUID_STRING_LENGTH || // Length check
            !String.valueOf(cuuid.chars).matches("^[0-9a-fA-F]*$") // Hex-only check
        ) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Searches for multiple CUUIDs in a given string and returns corresponding UUID[] on success or null on failure
     * */
    @Nullable
    public static UUID[] parseContinuousCUUIDString(@Nullable String cuuidString) {
        if (cuuidString == null) return null;
        int restOfChars = cuuidString.length();
        if (restOfChars < CUUID_STRING_LENGTH) return null;

        int cursor = 0;
        UUID uuid;
        List<UUID> nodeIds = new ArrayList<>();
        CUUID cuuid = new CUUID();

        while (restOfChars >= CUUID_STRING_LENGTH) {
            for (int i = 0; i < CUUID_STRING_LENGTH; ++i) {
                cuuid.chars[i] = cuuidString.charAt(cursor + i);
            }

            uuid = CUUID.toUUID(cuuid);
            if (uuid != null) nodeIds.add(uuid);
            restOfChars -= CUUID_STRING_LENGTH;
            cursor += CUUID_STRING_LENGTH;
        }

        return nodeIds.toArray(UUID[]::new);
    }

    /**
     * Searches for a single CUUID in a given string and returns corresponding UUID on success or null on failure
     * */
    @Nullable
    public static UUID parseSingleCUUIDString(@Nullable String cuuidString) {
        if (cuuidString == null) return null;
        if (cuuidString.length() == CUUID_STRING_LENGTH) {
            return CUUID.toUUID(new CUUID(cuuidString));
        }
        return null;
    }

    /**
     * Converts a CUUID[] to UUID[]
     * */
    @Nullable
    public static UUID[] CUUIDArrayToUUIDArray(@Nullable CUUID[] cuuids) {
        if (cuuids == null) return null;
        UUID[] uuids = new UUID[cuuids.length];
        for (int i = 0; i < cuuids.length; ++i) {
            uuids[i] = CUUID.toUUID(cuuids[i]);
        }
        return uuids;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CUUID) {
            for (int i = 0; i < CUUID_STRING_LENGTH; ++i) {
                if (((CUUID) obj).chars[i] != chars[i]) return false;
            }
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return String.valueOf(chars);
    }
}
