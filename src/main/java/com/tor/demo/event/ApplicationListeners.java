package com.tor.demo.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@Component
public class ApplicationListeners {
    private final JdbcTemplate jdbcTemplate;
    private static final Logger logger = LoggerFactory.getLogger(ApplicationListeners.class);

    public static final String DBScriptPath =
        System.getProperty("user.dir") + File.separatorChar +
        "src" + File.separatorChar +
        "main" + File.separatorChar +
        "resources" + File.separatorChar +
        "db" + File.separatorChar +
        "migration" + File.separatorChar;
    public static final String[] DBScripts = new String[] {
        "V1_Configuration.sql",
        "V2_CreateNodeTable.sql",
        "V3_CreateNodeGroupTable.sql",
        "V4_CreateAuthorityTable.sql"
    };

    @Autowired
    public ApplicationListeners(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void onApplicationReadyEvent() {
        configureDB(DBScriptPath, DBScripts);
    }

    /**
     * Runs the SQL scripts in DBScriptPath
     * */
    public int[] configureDB(String path, String[] fileNames) {
        if (fileNames == null || fileNames.length == 0) return null;

        // Path validity checks
        if (path == null || path.isBlank()) {
            path = "." + File.separatorChar;
        } else {
            if (path.matches("/") && '/' != File.separatorChar) {
                path.replace('/', File.separatorChar);
            }
            if (path.matches("\\\\") && '\\' != File.separatorChar) {
                path.replace('\\', File.separatorChar);
            }
            if (path.charAt(path.length() - 1) != File.separatorChar) {
                path += File.separatorChar;
            }
        }

        StringBuilder sql = new StringBuilder();
        List<String> linesBuffer;
        int lengthBuffer = 0;
        int[] results = new int[fileNames.length];

        for (int i = 0; i < fileNames.length; ++i) {
            logger.info("[" + fileNames[i] + "] - Running...");

            try {
                sql.delete(0, sql.length());
                linesBuffer = Files.readAllLines(Paths.get(path + fileNames[i]), StandardCharsets.UTF_8);
                lengthBuffer = linesBuffer.size();

                for (int j = 0; j < lengthBuffer; ++j) {
                    sql.append(linesBuffer.get(j));
                }

                try (Connection connection = jdbcTemplate.getDataSource().getConnection()) {
                    Statement stmt = connection.createStatement();
                    stmt.execute(sql.toString());
                    results[i] = 1;
                } catch (SQLException e) {
                    e.printStackTrace();
                    results[i] = 0;
                }
            } catch (IOException e) {
                e.printStackTrace();
                results[i] = 0;
            }

            if (results[i] == 1) {
                logger.info("[" + fileNames[i] + "] - SUCCESS");
            } else {
                logger.info("[" + fileNames[i] + "] - FAILURE");
            }
        }

        return results;
    }
}
