package com.tor.demo.dataaccess;

import com.tor.demo.model.Authority;
import com.tor.demo.util.CUUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
public class AuthorityDataAccessService {
    private final JdbcTemplate jdbcTemplate;

    public static final String TABLE = "authority";
    public static final String ID_COLUMN = "id";
    public static final String NAME_COLUMN = "name";
    public static final String URL_COLUMN = "url";
    public static final String CREATION_TIMESTAMPTZ_COLUMN = "creation_timestamptz";
    public static final String UPDATE_TIMESTAMPTZ_COLUMN = "update_timestamptz";

    @Autowired
    public AuthorityDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    // ---------- RowMappers ----------

    /**
     * STATUS: Reviewed
     *
     * This RowMapper is null-safe.
     * */
    private RowMapper<Authority> mapAuthorityFromDB() {
        return (rs, i) -> {
            String idString = rs.getString(1);
            String nameString = rs.getString(2);
            String urlString = rs.getString(3);
            Timestamp creationTimestampTZ = rs.getTimestamp(4);
            Timestamp updateTimestampTZ = rs.getTimestamp(5);

            return new Authority(
                (idString == null ? null : UUID.fromString(idString)),
                nameString,
                urlString,
                creationTimestampTZ,
                updateTimestampTZ
            );
        };
    }

    /**
     * STATUS: Reviewed
     *
     * This RowMapper is null-safe.
     * */
    @NotNull
    private RowMapper<UUID> mapUUIDFromDB() {
        return (rs, i) -> {
            String idString = rs.getString(1);
            return (idString == null ? null : UUID.fromString(idString));
        };
    }

    // ---------- GET ----------

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public Authority getAuthorityById(@NotNull UUID id) {
        String pgQuery =
            "SELECT * FROM " + TABLE + " " +
            "WHERE " + ID_COLUMN + " = ?::UUID";

        List<Authority> result;
        try {
            result = jdbcTemplate.query(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(pgQuery);
                    pstmt.setString(1, id.toString());
                    return pstmt;
                },
                mapAuthorityFromDB()
            );
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return result.size() > 0 ? result.get(0) : null;
    }

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public List<Authority> getAuthoritiesById(@NotNull UUID[] ids) {
        String pgQuery =
            "SELECT * FROM " + TABLE + " " +
            "WHERE " + ID_COLUMN + " = ANY (?::UUID[])";

        List<Authority> result;
        try {
            result = jdbcTemplate.query(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(pgQuery);
                    Array nodeIdSQLArray = connection.createArrayOf("UUID", ids);
                    pstmt.setArray(1, nodeIdSQLArray);
                    return pstmt;
                },
                mapAuthorityFromDB()
            );
        } catch (Exception e) {
            result = null;
            e.printStackTrace();
        }

        return result;
    }

    /**
     * STATUS: Reviewed
     *
     * TODO: Finish search flexibility implementation (current search in half-strict)
     * */
    @Nullable
    public List<Authority> getAuthoritiesByName(@NotNull String name) {
        String pgQuery =
            "SELECT * FROM " + TABLE + " " +
            "WHERE ?::VARCHAR ILIKE " + NAME_COLUMN;

        List<Authority> result;
        try {
            result = jdbcTemplate.query(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(pgQuery);
                    pstmt.setString(1, name);
                    pstmt.setString(2, name);
                    return pstmt;
                },
                mapAuthorityFromDB()
            );
        } catch (Exception e) {
            result = null;
            e.printStackTrace();
        }

        return result;
    }

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public List<Authority> getAllAuthorities(){
        List<Authority> result;
        try {
            result = jdbcTemplate.query(
                (connection) -> connection.prepareStatement("SELECT * FROM " + TABLE),
                mapAuthorityFromDB()
            );
        } catch (Exception e) {
            result = null;
            e.printStackTrace();
        }

        return result;
    }

    // ---------- POST ----------

    /**
     * STATUS: Reviewed
     *
     * Passed 'id', 'creationTimestampTZ', and 'lastUpdateTimestampTZ' are ignored
     * */
    @Nullable
    public String postAuthority(@Nullable Authority authority) {
        if (!Authority.isPostValid(authority)) return null;

        // Collision lookup
        String lookupQuery =
            "SELECT " + ID_COLUMN + " FROM " + TABLE + " " +
            "WHERE (?::TEXT = " + URL_COLUMN + ")";

        try {
            List<UUID> result = jdbcTemplate.query(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(lookupQuery);
                    pstmt.setString(1, authority.url);
                    return pstmt;
                },
                mapUUIDFromDB()
            );

            if (result.size() > 0) return null; // Collision check
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        String pgQuery =
            "INSERT INTO " + TABLE + " (" +
                ID_COLUMN + ", " +
                NAME_COLUMN + ", " +
                URL_COLUMN + ", " +
                CREATION_TIMESTAMPTZ_COLUMN + ", " +
                UPDATE_TIMESTAMPTZ_COLUMN +
            ") VALUES (" +
                "?::UUID, " +
                "?::VARCHAR, " +
                "?::TEXT, " +
                "?::TIMESTAMPTZ, " +
                "?::TIMESTAMPTZ" +
            ")";

        // Initialization checks and logic
        String id = UUID.randomUUID().toString();
        Timestamp creationTimestampTZ = Timestamp.from(Instant.now());
        Timestamp lastUpdateTimestampTZ = Timestamp.from(Instant.now());

        int affectedRows;
        try {
            affectedRows = jdbcTemplate.update(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(pgQuery);

                    pstmt.setString(1, id);
                    pstmt.setString(2, authority.name);
                    pstmt.setString(3, authority.url);
                    pstmt.setTimestamp(4, creationTimestampTZ);
                    pstmt.setTimestamp(5, lastUpdateTimestampTZ);

                    return pstmt;
                }
            );
        } catch (Exception e) {
            affectedRows = 0;
            e.printStackTrace();
        }

        return affectedRows == 1 ? id : null;
    }

    /**
     * STATUS: Reviewed
     *
     * TODO: Test Node null properties
     * Returns a list of amounts of affected rows with the same order as in 'groups'
     * */
    @NotNull
    public List<String> postAuthorities(@NotNull Authority[] authorities) {
        List<String> affectedRows = new ArrayList<>();
        for (int i = 0; i < authorities.length; ++i) affectedRows.add(postAuthority(authorities[i]));
        return affectedRows;
    }

    // ---------- UPDATE ----------

    /**
     * STATUS: Reviewed
     *
     * This method does not change 'creationTimestampTZ' and 'id'. It ignores provided 'update_timestamptz' and updates
     * the and fields specified, only if they were specified. If only 'update_timestamptz', 'creationTimestampTZ' and
     * 'id' are not null, method will not do anything.
     * */
    public void updateAuthorityById(@Nullable Authority authority) {
        if (authority == null || authority.id == null) return;

        boolean nameSpecified = authority.name != null;
        boolean urlSpecified = authority.url != null;

        // Check, if any meaningful data is specified
        if (!(nameSpecified || urlSpecified)) return;

        // Collision lookup
        String lookupQuery =
            "SELECT " + ID_COLUMN + " FROM " + TABLE + " " +
            "WHERE (?::TEXT = " + URL_COLUMN + ")";

        try {
            List<UUID> result = jdbcTemplate.query(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(lookupQuery);
                    pstmt.setString(1, authority.url);
                    return pstmt;
                },
                mapUUIDFromDB()
            );

            if (result.size() > 0) return; // Collision check
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        StringBuilder setClause = new StringBuilder();

        Timestamp lastUpdateTimestampTZ = Timestamp.from(Instant.now());
        String id = authority.id.toString();

        // Setting the query parameters
        if (nameSpecified) setClause.append(NAME_COLUMN + " = ?::VARCHAR");
        if (urlSpecified) {
            if (setClause.length() > 0) setClause.append(", "); // Delimiter check
            setClause.append(URL_COLUMN + " = ?::TEXT");
        }
        if (setClause.length() > 0) setClause.append(", "); // Delimiter check
        setClause.append(UPDATE_TIMESTAMPTZ_COLUMN + " = ?::TIMESTAMPTZ");

        // Forming the query string
        String pgQuery =
            "UPDATE " + TABLE + " " +
            "SET " + setClause.toString() + " " +
            "WHERE " + ID_COLUMN + " = ?::UUID";

        try {
            jdbcTemplate.update(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(pgQuery);
                    int columnsSpecified = 0;

                    // Setting the query arguments
                    if (nameSpecified) pstmt.setString(++columnsSpecified, authority.name);
                    if (urlSpecified) pstmt.setString(++columnsSpecified, authority.url);
                    pstmt.setTimestamp(++columnsSpecified, lastUpdateTimestampTZ);
                    pstmt.setString(++columnsSpecified, id);

                    return pstmt;
                }
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * STATUS: Reviewed
     * */
    public void updateAuthoritiesById(@NotNull Authority[] authorities) {
        for (int i = 0; i < authorities.length; ++i) updateAuthorityById(authorities[i]);
    }

    // ---------- DELETE ----------

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public Integer deleteAuthorityById(@NotNull UUID id) {
        String pgQuery =
            "DELETE FROM " + TABLE + " " +
            "WHERE " + ID_COLUMN + " = ?::UUID";

        int affectedRows = 0;
        try {
            affectedRows = jdbcTemplate.update(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(pgQuery);
                    pstmt.setString(1, id.toString());
                    return pstmt;
                }
            );
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return affectedRows;
    }

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public Integer deleteAuthoritiesById(@NotNull UUID[] ids) {
        String pgQuery =
            "DELETE FROM " + TABLE + " " +
            "WHERE " + ID_COLUMN + " = ANY (?::UUID[])";

        int affectedRows = 0;
        try {
            affectedRows = jdbcTemplate.update(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(pgQuery);
                    pstmt.setArray(1, connection.createArrayOf("UUID", ids));
                    return pstmt;
                }
            );
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return affectedRows;
    }
}
