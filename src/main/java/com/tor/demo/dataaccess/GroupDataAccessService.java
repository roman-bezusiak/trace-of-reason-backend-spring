package com.tor.demo.dataaccess;

import com.tor.demo.model.Group;
import com.tor.demo.util.CUUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
public class GroupDataAccessService {
    private final JdbcTemplate jdbcTemplate;

    public static final String TABLE = "node_group";
    public static final String ID_COLUMN = "id";
    public static final String NAME_COLUMN = "name";
    public static final String DESCRIPTION_COLUMN = "description";
    public static final String CREATION_TIMESTAMPTZ_COLUMN = "creation_timestamptz";
    public static final String UPDATE_TIMESTAMPTZ_COLUMN = "update_timestamptz";
    public static final String ALIASES_COLUMN = "aliases";
    public static final String AUTHORITY_IDS_COLUMN = "authority_ids";

    @Autowired
    public GroupDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    // ---------- RowMappers ----------

    /**
     * STATUS: Reviewed
     *
     * This RowMapper is null-safe.
     * */
    private RowMapper<Group> mapGroupFromDB() {
        return (rs, i) -> {
            String idString = rs.getString(1);
            String nameString = rs.getString(2);
            String descriptionString = rs.getString(3);
            Timestamp creationTimestampTZ = rs.getTimestamp(4);
            Timestamp updateTimestampTZ = rs.getTimestamp(5);
            Array aliasesArray = rs.getArray(6);
            Array authorityIdsArray = rs.getArray(7);

            return new Group(
                (idString == null ? null : UUID.fromString(idString)),
                nameString,
                descriptionString,
                creationTimestampTZ,
                updateTimestampTZ,
                (aliasesArray == null ? null : (String[]) aliasesArray.getArray()),
                (authorityIdsArray == null ? null : (UUID[]) authorityIdsArray.getArray())
            );
        };
    }

    /**
     * STATUS: Reviewed
     *
     * This RowMapper is null-safe.
     * */
    @NotNull
    private RowMapper<UUID> mapUUIDFromDB() {
        return (rs, i) -> {
            String idString = rs.getString(1);
            return (idString == null ? null : UUID.fromString(idString));
        };
    }

    // ---------- GET ----------

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public Group getGroupById(@NotNull UUID id) {
        String pgQuery =
            "SELECT * FROM " + TABLE + " " +
            "WHERE " + ID_COLUMN + " = ?::UUID";

        List<Group> result;
        try {
            result = jdbcTemplate.query(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(pgQuery);
                    pstmt.setString(1, id.toString());
                    return pstmt;
                },
                mapGroupFromDB()
            );
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return result.size() > 0 ? result.get(0) : null;
    }

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public List<Group> getGroupsById(@NotNull UUID[] ids) {
        String pgQuery =
            "SELECT * FROM " + TABLE + " " +
            "WHERE " + ID_COLUMN + " = ANY (?::UUID[])";

        List<Group> result;
        try {
            result = jdbcTemplate.query(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(pgQuery);
                    Array groupIdSQLArray = connection.createArrayOf("UUID", ids);
                    pstmt.setArray(1, groupIdSQLArray);
                    return pstmt;
                },
                mapGroupFromDB()
            );
        } catch (Exception e) {
            result = null;
            e.printStackTrace();
        }

        return result;
    }

    /**
     * STATUS: Reviewed
     *
     * TODO: Finish search flexibility implementation (current search in half-strict)
     * */
    @Nullable
    public List<Group> getGroupsByName(@NotNull String name) {
        String pgQuery =
            "SELECT * FROM " + TABLE + " " +
            "WHERE " +
                "?::VARCHAR ILIKE " + NAME_COLUMN + " OR " +
                "?::VARCHAR = ANY (" + ALIASES_COLUMN + ")";

        List<Group> result;
        try {
            result = jdbcTemplate.query(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(pgQuery);
                    pstmt.setString(1, name);
                    pstmt.setString(2, name);
                    return pstmt;
                },
                mapGroupFromDB()
            );
        } catch (Exception e) {
            result = null;
            e.printStackTrace();
        }

        return result;
    }

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public List<Group> getAllGroups(){
        List<Group> result;
        try {
            result = jdbcTemplate.query(
                (connection) -> connection.prepareStatement("SELECT * FROM " + TABLE),
                mapGroupFromDB()
            );
        } catch (Exception e) {
            result = null;
            e.printStackTrace();
        }

        return result;
    }

    // ---------- POST ----------

    /**
     * STATUS: Reviewed
     *
     * Passed 'id', 'creationTimestampTZ', and 'lastUpdateTimestampTZ' are ignored
     * */
    @Nullable
    public String postGroup(@Nullable Group group) {
        if (!Group.isPostValid(group)) return null;

        // Flags of optional node properties
        boolean aliasesSpecified = group.aliases != null;

        // Collision lookup
        String lookupQuery =
            "SELECT " + TABLE + "." + ID_COLUMN + " " +
            "FROM " + TABLE + ", " + AuthorityDataAccessService.TABLE + " " +
            "WHERE " +
                "(?::TEXT = " + TABLE + "." + DESCRIPTION_COLUMN + ") OR NOT " +
                "(" + AuthorityDataAccessService.TABLE + "." + AuthorityDataAccessService.ID_COLUMN + " = ANY (?::UUID[]))";

        try {
            List<UUID> result = jdbcTemplate.query(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(lookupQuery);
                    pstmt.setString(1, group.description);
                    pstmt.setArray(2, connection.createArrayOf("UUID", group.authorityIds));
                    return pstmt;
                },
                mapUUIDFromDB()
            );

            if (result.size() > 0) return null; // Collision check
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        StringBuilder queryColumns = new StringBuilder();
        StringBuilder queryParameters = new StringBuilder();
        String id = UUID.randomUUID().toString();
        Timestamp creationTimestampTZ = Timestamp.from(Instant.now());
        Timestamp lastUpdateTimestampTZ = Timestamp.from(Instant.now());

        queryColumns.append(ID_COLUMN);
        queryParameters.append("?::UUID");

        queryColumns.append(", " + NAME_COLUMN);
        queryParameters.append(", ?::VARCHAR");

        queryColumns.append(", " + DESCRIPTION_COLUMN);
        queryParameters.append(", ?::TEXT");

        queryColumns.append(", " + CREATION_TIMESTAMPTZ_COLUMN);
        queryParameters.append(", ?::TIMESTAMPTZ");

        queryColumns.append(", " + UPDATE_TIMESTAMPTZ_COLUMN);
        queryParameters.append(", ?::TIMESTAMPTZ");

        if (aliasesSpecified) {
            queryColumns.append(", " + ALIASES_COLUMN);
            queryParameters.append(", ?::TEXT[]");
        }

        queryColumns.append(", " + AUTHORITY_IDS_COLUMN);
        queryParameters.append(", ?::UUID[]");

        // Forming the query string
        String pgQuery =
            "INSERT INTO " + TABLE + " (" + queryColumns.toString() + ") " +
            "VALUES (" + queryParameters.toString() + ")";

        int affectedRows;
        try {
            affectedRows = jdbcTemplate.update(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(pgQuery);
                    int columnsSpecified = 0;

                    // Setting the query arguments
                    pstmt.setString(++columnsSpecified, id);
                    pstmt.setString(++columnsSpecified, group.name);
                    pstmt.setString(++columnsSpecified, group.description);
                    pstmt.setTimestamp(++columnsSpecified, creationTimestampTZ);
                    pstmt.setTimestamp(++columnsSpecified, lastUpdateTimestampTZ);
                    if (aliasesSpecified) pstmt.setArray(++columnsSpecified, connection.createArrayOf("VARCHAR", group.aliases));
                    pstmt.setArray(++columnsSpecified, connection.createArrayOf("UUID", group.authorityIds));

                    return pstmt;
                }
            );
        } catch (Exception e) {
            affectedRows = 0;
            e.printStackTrace();
        }

        return affectedRows == 1 ? id : null;
    }

    /**
     * STATUS: Reviewed
     *
     * TODO: Test Node null properties
     * Returns a list of amounts of affected rows with the same order as in 'groups'
     * */
    @NotNull
    public List<String> postGroups(@NotNull Group[] groups) {
        List<String> affectedRows = new ArrayList<>();
        for (int i = 0; i < groups.length; ++i) affectedRows.add(postGroup(groups[i]));
        return affectedRows;
    }

    // ---------- UPDATE ----------


    /**
     * Status: Reviewed
     *
     * This method does not change 'creationTimestampTZ' and 'id'. It ignores provided 'update_timestamptz' and updates
     * the and fields specified, only if they were specified. If only 'update_timestamptz', 'creationTimestampTZ' and
     * 'id' are not null, method will not do anything.
     * */
    public void updateGroupById(@Nullable Group group) {
        if (group == null || group.id == null) return;

        boolean nameSpecified = group.name != null;
        boolean descriptionSpecified = group.description != null;
        boolean aliasesSpecified = group.aliases != null;
        boolean authorityIdsSpecified = group.authorityIds != null;

        // Check, if any meaningful data is specified
        if (!(
            nameSpecified ||
            descriptionSpecified ||
            aliasesSpecified ||
            authorityIdsSpecified
        )) {
            return;
        }

        // Collision lookup
        String lookupQuery =
            "SELECT " + TABLE + "." + ID_COLUMN + " " +
            "FROM " + TABLE + ", " + AuthorityDataAccessService.TABLE + " " +
            "WHERE " +
                "(?::TEXT = " + TABLE + "." + DESCRIPTION_COLUMN + ") OR NOT " +
                "(" + AuthorityDataAccessService.TABLE + "." + AuthorityDataAccessService.ID_COLUMN + " = ANY (?::UUID[]))";

        try {
            List<UUID> result = jdbcTemplate.query(
                    (connection) -> {
                        PreparedStatement pstmt = connection.prepareStatement(lookupQuery);
                        pstmt.setString(1, group.description);
                        pstmt.setArray(2, connection.createArrayOf("UUID", group.authorityIds));
                        return pstmt;
                    },
                    mapUUIDFromDB()
            );

            if (result.size() > 0) return; // Collision check
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        StringBuilder setClause = new StringBuilder();
        Timestamp lastUpdateTimestampTZ = Timestamp.from(Instant.now());
        String id = group.id.toString();

        // Setting the query parameters
        if (nameSpecified) setClause.append(NAME_COLUMN + " = ?::VARCHAR");
        if (descriptionSpecified) {
            if (setClause.length() > 0) setClause.append(", "); // Delimiter check
            setClause.append(DESCRIPTION_COLUMN + " = ?::TEXT");
        }
        if (setClause.length() > 0) setClause.append(", "); // Delimiter check
        setClause.append(UPDATE_TIMESTAMPTZ_COLUMN + " = ?::TIMESTAMPTZ"); // After setting update_timestamptz, delimiter is always required
        if (aliasesSpecified) setClause.append(", " + ALIASES_COLUMN + " = ?::TEXT[]");
        if (authorityIdsSpecified) setClause.append(", " + AUTHORITY_IDS_COLUMN + " = ?::UUID[]");

        // Forming the query string
        String pgQuery =
            "UPDATE " + TABLE + " " +
            "SET " + setClause.toString() + " " +
            "WHERE " + ID_COLUMN + " = ?::UUID";

        try {
            jdbcTemplate.update(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(pgQuery);
                    int columnsSpecified = 0;

                    // Setting the query arguments
                    if (nameSpecified) pstmt.setString(++columnsSpecified, group.name);
                    if (descriptionSpecified) pstmt.setString(++columnsSpecified, group.description);
                    pstmt.setTimestamp(++columnsSpecified, lastUpdateTimestampTZ);
                    if (aliasesSpecified) pstmt.setArray(++columnsSpecified, connection.createArrayOf("VARCHAR", group.aliases));
                    if (authorityIdsSpecified) pstmt.setArray(++columnsSpecified, connection.createArrayOf("UUID", group.authorityIds));
                    pstmt.setString(++columnsSpecified, id);

                    return pstmt;
                }
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * STATUS: Reviewed
     * */
    public void updateGroupsById(@NotNull Group[] groups) {
        for (int i = 0; i < groups.length; ++i) updateGroupById(groups[i]);
    }

    // ---------- DELETE ----------

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public Integer deleteGroupById(@NotNull UUID id) {
        String pgQuery =
            "DELETE FROM " + TABLE + " " +
            "WHERE " + ID_COLUMN + " = ?::UUID";

        int affectedRows = 0;
        try {
            affectedRows = jdbcTemplate.update(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(pgQuery);
                    pstmt.setString(1, id.toString());
                    return pstmt;
                }
            );
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return affectedRows;
    }

    /**
     * STATUS: Reviewed
     * */
    @NotNull
    public Integer deleteGroupsById(@NotNull UUID[] ids) {
        String pgQuery =
            "DELETE FROM " + TABLE + " " +
            "WHERE " + ID_COLUMN + " = ANY (?::UUID[])";

        int affectedRows = 0;
        try {
            affectedRows = jdbcTemplate.update(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(pgQuery);
                    pstmt.setArray(1, connection.createArrayOf("UUID", ids));
                    return pstmt;
                }
            );
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return affectedRows;
    }
}
