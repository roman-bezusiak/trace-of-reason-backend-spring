package com.tor.demo.dataaccess;

import com.tor.demo.model.Node;
import com.tor.demo.util.CUUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
public class NodeDataAccessService {
    private final JdbcTemplate jdbcTemplate;

    public static final String TABLE = "node";
    public static final String ID_COLUMN = "id";
    public static final String NAME_COLUMN = "name";
    public static final String URL_COLUMN = "url";
    public static final String CREATION_TIMESTAMPTZ_COLUMN = "creation_timestamptz";
    public static final String UPDATE_TIMESTAMPTZ_COLUMN = "update_timestamptz";
    public static final String ALIASES_COLUMN = "aliases";
    public static final String AUTHORITY_IDS_COLUMN = "authority_ids";
    public static final String GROUP_IDS_COLUMN = "group_ids";
    public static final String PARENT_IDS_COLUMN = "parent_ids";
    public static final String CHILD_IDS_COLUMN = "child_ids";

    @Autowired
    public NodeDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    // ---------- RowMappers ----------

    /**
     * STATUS: Reviewed
     *
     * This RowMapper is null-safe.
     * */
    @NotNull
    private RowMapper<Node> mapNodeFromDB() {
        return (rs, i) -> {
            String idString = rs.getString(1);
            String nameString = rs.getString(2);
            String urlString = rs.getString(3);
            Timestamp creationTimestampTZ = rs.getTimestamp(4);
            Timestamp updateTimestampTZ = rs.getTimestamp(5);
            Array aliasesArray = rs.getArray(6);
            Array authorityIdsArray = rs.getArray(7);
            Array groupIdsArray = rs.getArray(8);
            Array parentIdsArray = rs.getArray(9);
            Array childIdsArray = rs.getArray(10);

            return new Node(
                (idString == null ? null : UUID.fromString(idString)),
                nameString,
                urlString,
                creationTimestampTZ,
                updateTimestampTZ,
                (aliasesArray == null ? null : (String[]) aliasesArray.getArray()),
                (authorityIdsArray == null ? null : (UUID[]) authorityIdsArray.getArray()),
                (groupIdsArray == null ? null : (UUID[]) groupIdsArray.getArray()),
                (parentIdsArray == null ? null : (UUID[]) parentIdsArray.getArray()),
                (childIdsArray == null ? null : (UUID[]) childIdsArray.getArray())
            );
        };
    }

    /**
     * STATUS: Reviewed
     *
     * This RowMapper is null-safe.
     * */
    @NotNull
    private RowMapper<UUID> mapUUIDFromDB() {
        return (rs, i) -> {
            String idString = rs.getString(1);
            return (idString == null ? null : UUID.fromString(idString));
        };
    }

    // ---------- GET ----------

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public Node getNodeById(@NotNull UUID id) {
        String pgQuery =
            "SELECT * FROM " + TABLE + " " +
            "WHERE " + ID_COLUMN + " = ?::UUID";

        List<Node> result;
        try {
            result = jdbcTemplate.query(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(pgQuery);
                    pstmt.setString(1, id.toString());
                    return pstmt;
                },
                mapNodeFromDB()
            );
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return result.size() > 0 ? result.get(0) : null;
    }

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public List<Node> getNodesById(@NotNull UUID[] ids) {
        String pgQuery =
            "SELECT * FROM " + TABLE + " " +
            "WHERE " + ID_COLUMN + " = ANY (?::UUID[])";

        List<Node> result;
        try {
            result = jdbcTemplate.query(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(pgQuery);
                    Array nodeIdSQLArray = connection.createArrayOf("UUID", ids);
                    pstmt.setArray(1, nodeIdSQLArray);
                    return pstmt;
                },
                mapNodeFromDB()
            );
        } catch (Exception e) {
            result = null;
            e.printStackTrace();
        }

        return result;
    }

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public List<Node> getNodesByGroupId(@NotNull UUID id) {
        String pgQuery =
            "SELECT * FROM " + TABLE + " " +
            "WHERE ?::UUID = ANY (" + GROUP_IDS_COLUMN + ")";

        List<Node> result;
        try {
            result = jdbcTemplate.query(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(pgQuery);
                    pstmt.setString(1, id.toString());
                    return pstmt;
                },
                mapNodeFromDB()
            );
        } catch (Exception e) {
            result = null;
            e.printStackTrace();
        }

        return result;
    }

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public List<Node> getNodesByAuthorityId(@NotNull UUID id) {
        String pgQuery =
            "SELECT * FROM " + TABLE + " " +
            "WHERE ?::UUID = ANY (" + AUTHORITY_IDS_COLUMN + ")";

        List<Node> result;
        try {
            result = jdbcTemplate.query(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(pgQuery);
                    pstmt.setString(1, id.toString());
                    return pstmt;
                },
                mapNodeFromDB()
            );
        } catch (Exception e) {
            result = null;
            e.printStackTrace();
        }

        return result;
    }

    /**
     * STATUS: Reviewed
     *
     * TODO: Finish search flexibility implementation (current search in half-strict)
     * */
    @Nullable
    public List<Node> getNodesByName(@NotNull String name) {
        String pgQuery =
            "SELECT * FROM " + TABLE + " " +
            "WHERE " +
                "?::VARCHAR ILIKE " + NAME_COLUMN + " OR " +
                "?::VARCHAR = ANY (" + ALIASES_COLUMN + ")";

        List<Node> result;
        try {
            result = jdbcTemplate.query(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(pgQuery);
                    pstmt.setString(1, name);
                    pstmt.setString(2, name);
                    return pstmt;
                },
                mapNodeFromDB()
            );
        } catch (Exception e) {
            result = null;
            e.printStackTrace();
        }

        return result;
    }

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public List<Node> getAllNodes() {
        List<Node> result;
        try {
            result = jdbcTemplate.query(
                (connection) -> connection.prepareStatement("SELECT * FROM " + TABLE),
                mapNodeFromDB()
            );
        } catch (Exception e) {
            result = null;
            e.printStackTrace();
        }

        return result;
    }

    // ---------- POST ----------

    /**
     * STATUS: Reviewed
     *
     * Passed 'id', 'creationTimestampTZ', and 'lastUpdateTimestampTZ' are ignored
     * */
    @Nullable
    public String postNode(@Nullable Node node) {
        if (!Node.isPostValid(node)) return null;

        // Flags of optional node properties
        boolean aliasesSpecified = node.aliases != null;
        boolean groupIdsSpecified = node.groupIds != null;
        boolean parentIdsSpecified = node.parentIds != null;
        boolean childIdsSpecified = node.childIds != null;

        // Collision lookup
        String lookupQuery =
            "SELECT " + TABLE + "." + ID_COLUMN + " " +
            "FROM " +
                TABLE + ", " +
                AuthorityDataAccessService.TABLE + ", " +
                GroupDataAccessService.TABLE + " " +
            "WHERE " +
                "(?::TEXT = " + TABLE + "." + URL_COLUMN + ")" +
                " OR NOT (" + AuthorityDataAccessService.TABLE + "." + AuthorityDataAccessService.ID_COLUMN + " = ANY (?::UUID[]))" +
                (groupIdsSpecified ? " OR NOT (" + GroupDataAccessService.TABLE + "." + GroupDataAccessService.ID_COLUMN + " = ANY (?::UUID[]))" : "") +
                (parentIdsSpecified ? " OR NOT (" + TABLE + "." + ID_COLUMN + " = ANY (?::UUID[]))" : "") +
                (childIdsSpecified ? " OR NOT (" + TABLE + "." + ID_COLUMN + " = ANY (?::UUID[]))" : "");

        try {
            List<UUID> result = jdbcTemplate.query(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(lookupQuery);
                    int parameterCount = 0;

                    // Setting the insertion condition arguments
                    pstmt.setString(++parameterCount, node.url);
                    pstmt.setArray(++parameterCount, connection.createArrayOf("UUID", node.authorityIds));
                    if (groupIdsSpecified) pstmt.setArray(++parameterCount, connection.createArrayOf("UUID", node.groupIds));
                    if (parentIdsSpecified) pstmt.setArray(++parameterCount, connection.createArrayOf("UUID", node.parentIds));
                    if (childIdsSpecified) pstmt.setArray(++parameterCount, connection.createArrayOf("UUID", node.childIds));

                    return pstmt;
                },
                mapUUIDFromDB()
            );

            if (result.size() > 0) return null; // Collision check
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        StringBuilder insertionColumns = new StringBuilder();
        StringBuilder insertionParameters = new StringBuilder();
        String id = UUID.randomUUID().toString();
        Timestamp creationTimestampTZ = Timestamp.from(Instant.now());
        Timestamp lastUpdateTimestampTZ = Timestamp.from(Instant.now());

        // Setting the query parameters

        insertionColumns.append(ID_COLUMN);
        insertionParameters.append("?::UUID");

        insertionColumns.append(", " + NAME_COLUMN);
        insertionParameters.append(", ?::VARCHAR");

        insertionColumns.append(", " + URL_COLUMN);
        insertionParameters.append(", ?::TEXT");

        insertionColumns.append(", " + CREATION_TIMESTAMPTZ_COLUMN);
        insertionParameters.append(", ?::TIMESTAMPTZ");

        insertionColumns.append(", " + UPDATE_TIMESTAMPTZ_COLUMN);
        insertionParameters.append(", ?::TIMESTAMPTZ");

        if (aliasesSpecified) {
            insertionColumns.append(", " + ALIASES_COLUMN);
            insertionParameters.append(", ?::TEXT[]");
        }

        insertionColumns.append(", " + AUTHORITY_IDS_COLUMN);
        insertionParameters.append(", ?::UUID[]");

        if (groupIdsSpecified) {
            insertionColumns.append(", " + GROUP_IDS_COLUMN);
            insertionParameters.append(", ?::UUID[]");
        }

        if (parentIdsSpecified) {
            insertionColumns.append(", " + PARENT_IDS_COLUMN);
            insertionParameters.append(", ?::UUID[]");
        }

        if (childIdsSpecified) {
            insertionColumns.append(", " + CHILD_IDS_COLUMN);
            insertionParameters.append(", ?::UUID[]");
        }

        // Forming the query string
        String pgQuery =
            "INSERT INTO " + TABLE + " (" + insertionColumns.toString() + ") " +
            "VALUES (" + insertionParameters.toString() + ")";

        int affectedRows;
        try {
            affectedRows = jdbcTemplate.update(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(pgQuery);
                    int parameterCount = 0;

                    // Setting the insertion value arguments
                    pstmt.setString(++parameterCount, id);
                    pstmt.setString(++parameterCount, node.name);
                    pstmt.setString(++parameterCount, node.url);
                    pstmt.setTimestamp(++parameterCount, creationTimestampTZ);
                    pstmt.setTimestamp(++parameterCount, lastUpdateTimestampTZ);
                    if (aliasesSpecified) pstmt.setArray(++parameterCount, connection.createArrayOf("VARCHAR", node.aliases));
                    pstmt.setArray(++parameterCount, connection.createArrayOf("UUID", node.authorityIds));
                    if (groupIdsSpecified) pstmt.setArray(++parameterCount, connection.createArrayOf("UUID", node.groupIds));
                    if (parentIdsSpecified) pstmt.setArray(++parameterCount, connection.createArrayOf("UUID", node.parentIds));
                    if (childIdsSpecified) pstmt.setArray(++parameterCount, connection.createArrayOf("UUID", node.childIds));

                    return pstmt;
                }
            );
        } catch (Exception e) {
            affectedRows = 0;
            e.printStackTrace();
        }

        return affectedRows == 1 ? id : null;
    }

    /**
     * STATUS: Reviewed
     *
     * TODO: Test Node null properties
     * Returns a list of amounts of affected rows with the same order as in 'nodes'
     * */
    @NotNull
    public List<String> postNodes(@NotNull Node[] nodes) {
        List<String> results = new ArrayList<>();
        for (int i = 0; i < nodes.length; ++i) results.add(postNode(nodes[i]));
        return results;
    }

    // ---------- UPDATE ----------

    /**
     * STATUS: Reviewed
     *
     * This method does not change 'creationTimestampTZ' and 'id'. It ignores provided 'update_timestamptz' and updates
     * the and fields specified, only if they were specified. If only 'update_timestamptz', 'creationTimestampTZ' and
     * 'id' are not null, method will not do anything.
     * */
    public void updateNodeById(@Nullable Node node) {
        if (node == null || node.id == null) return;

        boolean nameSpecified = node.name != null;
        boolean urlSpecified = node.url != null;
        boolean aliasesSpecified = node.aliases != null;
        boolean authorityIdsSpecified = node.authorityIds != null;
        boolean groupIdsSpecified = node.groupIds != null;
        boolean parentIdsSpecified = node.parentIds != null;
        boolean childIdsSpecified = node.childIds != null;

        // Check, if any meaningful data is specified
        if (!(
            nameSpecified ||
            urlSpecified ||
            aliasesSpecified ||
            authorityIdsSpecified ||
            groupIdsSpecified ||
            parentIdsSpecified ||
            childIdsSpecified
        )) return;

        // Collision lookup query
        String lookupQuery =
            "SELECT " + TABLE + "." + ID_COLUMN + " " +
            "FROM " +
                TABLE + ", " +
                AuthorityDataAccessService.TABLE + ", " +
                GroupDataAccessService.TABLE + " " +
            "WHERE " +
                "(?::TEXT = " + TABLE + "." + URL_COLUMN + ")" +
                " OR NOT (" + AuthorityDataAccessService.TABLE + "." + AuthorityDataAccessService.ID_COLUMN + " = ANY (?::UUID[]))" +
                (groupIdsSpecified ? " OR NOT (" + GroupDataAccessService.TABLE + "." + GroupDataAccessService.ID_COLUMN + " = ANY (?::UUID[]))" : "") +
                (parentIdsSpecified ? " OR NOT (" + TABLE + "." + ID_COLUMN + " = ANY (?::UUID[]))" : "") +
                (childIdsSpecified ? " OR NOT (" + TABLE + "." + ID_COLUMN + " = ANY (?::UUID[]))" : "");

        try {
            List<UUID> result = jdbcTemplate.query(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(lookupQuery);
                    int parameterCount = 0;

                    // Setting the insertion condition arguments
                    pstmt.setString(++parameterCount, node.url);
                    pstmt.setArray(++parameterCount, connection.createArrayOf("UUID", node.authorityIds));
                    if (groupIdsSpecified) pstmt.setArray(++parameterCount, connection.createArrayOf("UUID", node.groupIds));
                    if (parentIdsSpecified) pstmt.setArray(++parameterCount, connection.createArrayOf("UUID", node.parentIds));
                    if (childIdsSpecified) pstmt.setArray(++parameterCount, connection.createArrayOf("UUID", node.childIds));

                    return pstmt;
                },
                mapUUIDFromDB()
            );

            if (result.size() > 0) return; // Collision check
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        StringBuilder setClause = new StringBuilder();
        Timestamp lastUpdateTimestampTZ = Timestamp.from(Instant.now());
        String id = node.id.toString();

        // Setting the query parameters
        if (nameSpecified) setClause.append(NAME_COLUMN + " = ?::VARCHAR");
        if (urlSpecified) {
            if (setClause.length() > 0) setClause.append(", "); // Delimiter check
            setClause.append(URL_COLUMN + " = ?::TEXT");
        }
        if (setClause.length() > 0) setClause.append(", "); // Delimiter check
        setClause.append(UPDATE_TIMESTAMPTZ_COLUMN + " = ?::TIMESTAMPTZ"); // After setting update_timestamptz, delimiter is always required
        if (aliasesSpecified) setClause.append(", " + ALIASES_COLUMN + " = ?::TEXT[]");
        if (authorityIdsSpecified) setClause.append(", " + AUTHORITY_IDS_COLUMN + " = ?::UUID[]");
        if (groupIdsSpecified) setClause.append(", " + GROUP_IDS_COLUMN + " = ?::UUID[]");
        if (parentIdsSpecified) setClause.append(", " + PARENT_IDS_COLUMN + " = ?::UUID[]");
        if (childIdsSpecified) setClause.append(", " + CHILD_IDS_COLUMN + " = ?::UUID[]");

        // Forming the query string
        String pgQuery =
            "UPDATE " + TABLE + " " +
            "SET " + setClause.toString() + " " +
            "WHERE " + ID_COLUMN + " = ?::UUID";

        try {
            jdbcTemplate.update(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(pgQuery);
                    int columnsSpecified = 0;

                    // Setting the query arguments
                    if (nameSpecified) pstmt.setString(++columnsSpecified, node.name);
                    if (urlSpecified) pstmt.setString(++columnsSpecified, node.url);
                    pstmt.setTimestamp(++columnsSpecified, lastUpdateTimestampTZ);
                    if (aliasesSpecified) pstmt.setArray(++columnsSpecified, connection.createArrayOf("VARCHAR", node.aliases));
                    if (authorityIdsSpecified) pstmt.setArray(++columnsSpecified, connection.createArrayOf("UUID", node.authorityIds));
                    if (groupIdsSpecified) pstmt.setArray(++columnsSpecified, connection.createArrayOf("UUID", node.groupIds));
                    if (parentIdsSpecified) pstmt.setArray(++columnsSpecified, connection.createArrayOf("UUID", node.parentIds));
                    if (childIdsSpecified) pstmt.setArray(++columnsSpecified, connection.createArrayOf("UUID", node.childIds));
                    pstmt.setString(++columnsSpecified, id);

                    return pstmt;
                }
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * STATUS: Reviewed
     * */
    public void updateNodesById(@NotNull Node[] nodes) {
        for (int i = 0; i < nodes.length; ++i) updateNodeById(nodes[i]);
    }

    // ---------- DELETE ----------

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public Integer deleteNodeById(@NotNull UUID id) {
        String pgQuery =
            "DELETE FROM " + TABLE + " " +
            "WHERE " + ID_COLUMN + " = ?::UUID";

        int affectedRows = 0;
        try {
            affectedRows = jdbcTemplate.update(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(pgQuery);
                    pstmt.setString(1, id.toString());
                    return pstmt;
                }
            );
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return affectedRows;
    }

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public Integer deleteNodesById(@NotNull UUID[] ids) {
        String pgQuery =
            "DELETE FROM " + TABLE + " " +
            "WHERE " + ID_COLUMN + " = ANY (?::UUID[])";

        int affectedRows = 0;
        try {
            affectedRows = jdbcTemplate.update(
                (connection) -> {
                    PreparedStatement pstmt = connection.prepareStatement(pgQuery);
                    pstmt.setArray(1, connection.createArrayOf("UUID", ids));
                    return pstmt;
                }
            );
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return affectedRows;
    }
}
