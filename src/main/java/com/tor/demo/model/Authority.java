package com.tor.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tor.demo.util.CUUID;
import org.springframework.data.annotation.Id;
import org.springframework.lang.Nullable;

import java.sql.Timestamp;
import java.util.UUID;

public class Authority {
    @Id public UUID id = null;
    public String name = null;
    public String url = null;
    public Timestamp creationTimestampTZ = null;
    public Timestamp updateTimestampTZ = null;

    /**
     * Continuous initialization
     * */
    public Authority() {}

    /**
     * Test stub initialization
     * */
    public Authority(
        String name,
        String url
    ) {
        this.name = name;
        this.url = url;
    }

    /**
     * DB mapping
     * */
    public Authority(
        @Nullable @JsonProperty("id") UUID id,
        @JsonProperty("name") String name,
        @JsonProperty("url") String url,
        @Nullable @JsonProperty("creationTimestampTZ") Timestamp creationTimestampTZ,
        @Nullable @JsonProperty("updateTimestampTZ") Timestamp updateTimestampTZ
    ) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.creationTimestampTZ = creationTimestampTZ;
        this.updateTimestampTZ = updateTimestampTZ;
    }

    /**
     * Controller @RequestBody mapping
     * */
    public Authority(
            @Nullable @JsonProperty("id") CUUID id,
            @JsonProperty("name") String name,
            @JsonProperty("url") String url,
            @Nullable @JsonProperty("creationTimestampTZ") Timestamp creationTimestampTZ,
            @Nullable @JsonProperty("updateTimestampTZ") Timestamp updateTimestampTZ
    ) {
        this.id = CUUID.toUUID(id);
        this.name = name;
        this.url = url;
        this.creationTimestampTZ = creationTimestampTZ;
        this.updateTimestampTZ = updateTimestampTZ;
    }

    /**
     * Checks whether the object satisfies minimum requirements for POST method
     * */
    @Nullable
    public static boolean isPostValid(@Nullable Authority authority) {
        return (
            authority != null &&
            authority.name != null &&
            authority.url != null
        );
    }
}
