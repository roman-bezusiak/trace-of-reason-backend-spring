package com.tor.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tor.demo.util.CUUID;
import org.springframework.data.annotation.Id;
import org.springframework.lang.Nullable;

import java.sql.Timestamp;
import java.util.UUID;

public class Group {
    @Id public UUID id = null;
    public String name = null;
    public String description = null;
    public Timestamp creationTimestampTZ = null;
    public Timestamp updateTimestampTZ = null;
    public String[] aliases = null;
    public UUID[] authorityIds = null;

    /**
     * Continuous initialization
     * */
    public Group() {}

    /**
     * Test stub initialization
     * */
    public Group(
        String name,
        String description,
        UUID[] authorityIds
    ) {
        this.name = name;
        this.description = description;
        this.authorityIds = authorityIds;
    }

    /**
     * DB mapping
     * */
    public Group(
        @Nullable @JsonProperty("id") UUID id,
        @JsonProperty("name") String name,
        @JsonProperty("description") String description,
        @Nullable @JsonProperty("creationTimestampTZ") Timestamp creationTimestampTZ,
        @Nullable @JsonProperty("updateTimestampTZ") Timestamp updateTimestampTZ,
        @Nullable @JsonProperty("aliases") String[] aliases,
        @JsonProperty("authorityIds") UUID[] authorityIds
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.creationTimestampTZ = creationTimestampTZ;
        this.updateTimestampTZ = updateTimestampTZ;
        this.aliases = aliases;
        this.authorityIds = authorityIds;
    }

    /**
     * Controller @RequestBody mapping
     * */
    public Group(
        @Nullable @JsonProperty("id") CUUID id,
        @JsonProperty("name") String name,
        @JsonProperty("description") String description,
        @Nullable @JsonProperty("creationTimestampTZ") Timestamp creationTimestampTZ,
        @Nullable @JsonProperty("updateTimestampTZ") Timestamp updateTimestampTZ,
        @Nullable @JsonProperty("aliases") String[] aliases,
        @JsonProperty("authorityIds") CUUID[] authorityIds
    ) {
        this.id = CUUID.toUUID(id);
        this.name = name;
        this.description = description;
        this.creationTimestampTZ = creationTimestampTZ;
        this.updateTimestampTZ = updateTimestampTZ;
        this.aliases = aliases;
        this.authorityIds = CUUID.CUUIDArrayToUUIDArray(authorityIds);
    }

    /**
     * Checks whether the object satisfies minimum requirements for POST method
     * */
    @Nullable
    public static boolean isPostValid(@Nullable Group group) {
        return (
            group != null &&
            group.name != null &&
            group.description != null &&
            group.authorityIds != null
        );
    }
}
