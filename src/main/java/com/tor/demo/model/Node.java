package com.tor.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tor.demo.util.CUUID;
import org.springframework.data.annotation.Id;
import org.springframework.lang.Nullable;

import java.sql.Timestamp;
import java.util.UUID;

public class Node {
    @Id public UUID id = null;
    public String name = null;
    public String url = null;
    public Timestamp creationTimestampTZ = null;
    public Timestamp updateTimestampTZ = null;
    public String[] aliases = null;
    public UUID[] authorityIds = null;
    public UUID[] groupIds = null;
    public UUID[] parentIds = null;
    public UUID[] childIds = null;

    /**
     * Continuous initialization
     * */
    public Node() {}

    /**
     * Test stub initialization
     * */
    public Node(
        String name,
        String url,
        UUID[] authorityIds
    ) {
        this.name = name;
        this.url = url;
        this.authorityIds = authorityIds;
    }

    /**
     * DB mapping
     * */
    public Node(
        @Nullable @JsonProperty("id") UUID id,
        @JsonProperty("name") String name,
        @JsonProperty("url") String url,
        @Nullable @JsonProperty("creationTimestampTZ") Timestamp creationTimestampTZ,
        @Nullable @JsonProperty("updateTimestampTZ") Timestamp updateTimestampTZ,
        @Nullable @JsonProperty("aliases") String[] aliases,
        @JsonProperty("authorityIds") UUID[] authorityIds,
        @Nullable @JsonProperty("groupIds") UUID[] groupIds,
        @Nullable @JsonProperty("parentIds") UUID[] parentIds,
        @Nullable @JsonProperty("childIds") UUID[] childIds
    ) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.creationTimestampTZ = creationTimestampTZ;
        this.updateTimestampTZ = updateTimestampTZ;
        this.aliases = aliases;
        this.authorityIds = authorityIds;
        this.groupIds = groupIds;
        this.parentIds = parentIds;
        this.childIds = childIds;
    }

    /**
     * Controller @RequestBody mapping
     * */
    public Node(
        @Nullable @JsonProperty("id") CUUID id,
        @JsonProperty("name") String name,
        @JsonProperty("url") String url,
        @Nullable @JsonProperty("creationTimestampTZ") Timestamp creationTimestampTZ,
        @Nullable @JsonProperty("updateTimestampTZ") Timestamp updateTimestampTZ,
        @Nullable @JsonProperty("aliases") String[] aliases,
        @JsonProperty("authorityIds") CUUID[] authorityIds,
        @Nullable @JsonProperty("groupIds") CUUID[] groupIds,
        @Nullable @JsonProperty("parentIds") CUUID[] parentIds,
        @Nullable @JsonProperty("childIds") CUUID[] childIds
    ) {
        this.id = CUUID.toUUID(id);
        this.name = name;
        this.url = url;
        this.creationTimestampTZ = creationTimestampTZ;
        this.updateTimestampTZ = updateTimestampTZ;
        this.aliases = aliases;
        this.authorityIds = CUUID.CUUIDArrayToUUIDArray(authorityIds);
        this.groupIds = CUUID.CUUIDArrayToUUIDArray(groupIds);
        this.parentIds = CUUID.CUUIDArrayToUUIDArray(parentIds);
        this.childIds = CUUID.CUUIDArrayToUUIDArray(childIds);
    }

    /**
     * Checks whether the object satisfies minimum requirements for POST method
     * */
    @Nullable
    public static boolean isPostValid(@Nullable Node node) {
        return (
            node != null &&
            node.name != null &&
            node.url != null &&
            node.authorityIds != null
        );
    }
}
