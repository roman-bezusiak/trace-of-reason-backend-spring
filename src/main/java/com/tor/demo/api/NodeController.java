package com.tor.demo.api;

import com.tor.demo.model.Node;
import com.tor.demo.service.NodeLogicService;
import com.tor.demo.util.CUUID;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(tags = "Node CRUD")
@RestController
@RequestMapping("api/")
class NodeController {
    private final NodeLogicService nodeLogicService;

    @Autowired
    public NodeController(NodeLogicService nodeLogicService) {
        this.nodeLogicService = nodeLogicService;
    }

    // ---------- GET ----------

    @Nullable
    @ApiOperation(value = "Returns a node found by node CUUID")
    @GetMapping(path = "node/{cuuid}")
    public Node getNodeById(@Nullable @PathVariable("cuuid") String cuuid) {
        UUID id = CUUID.parseSingleCUUIDString(cuuid);
        return id == null
            ? null
            : nodeLogicService.getNodeById(id);
    }

    @Nullable
    @ApiOperation(value = "Returns nodes found by node CUUIDs")
    @GetMapping(path = "nodes/{cuuids}")
    public List<Node> getNodesById(@Nullable @PathVariable("cuuids") String cuuids) {
        UUID[] ids = CUUID.parseContinuousCUUIDString(cuuids);
        return ids == null
            ? null
            : nodeLogicService.getNodesById(ids);
    }

    @Nullable
    @ApiOperation(value = "Returns nodes found by group CUUID")
    @GetMapping(path = "nodes/group/{cuuid}")
    public List<Node> getNodesByGroupId(@Nullable @PathVariable("cuuid") String cuuid) {
        UUID id = CUUID.parseSingleCUUIDString(cuuid);
        return id == null
            ? null
            : nodeLogicService.getNodesByGroupId(id);
    }

    @Nullable
    @ApiOperation(value = "Returns nodes found by authority CUUID")
    @GetMapping(path = "nodes/authority/{cuuid}")
    public List<Node> getNodesByAuthorityId(@Nullable @PathVariable("cuuid") String cuuid) {
        UUID id = CUUID.parseSingleCUUIDString(cuuid);
        return id == null
            ? null
            : nodeLogicService.getNodesByAuthorityId(id);
    }

    @Nullable
    @ApiOperation(value = "Returns nodes found by node name or node alias")
    @GetMapping(path = "nodes/name/{name}")
    public List<Node> getNodesByName(@Nullable @PathVariable("name") String name) {
        return (name == null || name.isBlank())
            ? null
            : nodeLogicService.getNodesByName(name);
    }

    @Nullable
    @ApiOperation(value = "Returns all nodes from DB")
    @GetMapping(path = "nodes")
    public List<Node> getAllNodes() {
        return nodeLogicService.getAllNodes();
    }

    // ---------- POST ----------

    /**
     * Null checks are done on DataAccess layer
     * */
    @Nullable
    @ApiOperation(value = "Inserts a node into DB from provided node")
    @PostMapping(path = "node")
    public String postNode(@Nullable @RequestBody Node node) {
        return nodeLogicService.postNode(node);
    }

    @Nullable
    @ApiOperation(value = "Inserts nodes into DB from provided nodes")
    @PostMapping(path = "nodes")
    public List<String> postNodes(@Nullable @RequestBody Node[] nodes) {
        return (nodes == null || nodes.length == 0)
            ? null
            : nodeLogicService.postNodes(nodes);
    }

    // ---------- UPDATE ----------

    /**
     * Null checks are done on DataAccess layer
     * */
    @ApiOperation(value = "Updates a node by UUID from provided node")
    @PutMapping(path = "node")
    public void updateNodeById(@Nullable @RequestBody Node node) {
        nodeLogicService.updateNodeById(node);
    }

    @ApiOperation(value = "Updates nodes by UUIDs from provided nodes")
    @PutMapping(path = "nodes")
    public void updateNodesById(@Nullable @RequestBody Node[] nodes) {
        if (nodes == null || nodes.length == 0) return;
        nodeLogicService.updateNodesById(nodes);
    }

    // ---------- DELETE ----------

    @Nullable
    @ApiOperation(value = "Deletes a node by node CUUID")
    @DeleteMapping("node/{cuuid}")
    public Integer deleteNodeById(@Nullable @PathVariable("cuuid") CUUID cuuid) {
        UUID id = CUUID.toUUID(cuuid);
        if (id == null) return null;
        return nodeLogicService.deleteNodeById(id);
    }

    @Nullable
    @ApiOperation(value = "Deletes nodes by node CUUIDs")
    @DeleteMapping("nodes/{cuuids}")
    public Integer deleteNodesById(@Nullable @PathVariable("cuuids") String cuuids) {
        UUID[] ids = CUUID.parseContinuousCUUIDString(cuuids);
        if (ids == null) return null;
        return nodeLogicService.deleteNodesById(ids);
    }
}