package com.tor.demo.api;

import com.tor.demo.model.Authority;
import com.tor.demo.service.AuthorityLogicService;
import com.tor.demo.util.CUUID;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(tags = "Authority CRUD")
@RestController
@RequestMapping("api/")
class AuthorityController {
    private final AuthorityLogicService authorityLogicService;

    @Autowired
    public AuthorityController(AuthorityLogicService authorityLogicService) {
        this.authorityLogicService = authorityLogicService;
    }

    // ---------- GET ----------

    @Nullable
    @ApiOperation(value = "Returns an authority found by authority CUUID")
    @GetMapping(path = "authority/{cuuid}")
    public Authority getAuthorityById(@Nullable @PathVariable("cuuid") String cuuid) {
        UUID id = CUUID.parseSingleCUUIDString(cuuid);
        return id == null
            ? null
            : authorityLogicService.getAuthorityById(id);
    }

    @Nullable
    @ApiOperation(value = "Returns authorities found by authority CUUIDs")
    @GetMapping(path = "authorities/{cuuids}")
    public List<Authority> getAuthoritiesById(@Nullable @PathVariable("cuuids") String cuuids) {
        UUID[] ids = CUUID.parseContinuousCUUIDString(cuuids);
        return ids == null
            ? null
            : authorityLogicService.getAuthoritiesById(ids);
    }

    @Nullable
    @ApiOperation(value = "Returns authorities found by authority name")
    @GetMapping(path = "authorities/name/{name}")
    public List<Authority> getAuthoritiesByName(@Nullable @PathVariable("name") String name) {
        return (name == null || name.isBlank())
            ? null
            : authorityLogicService.getAuthoritiesByName(name);
    }

    @Nullable
    @ApiOperation(value = "Returns all authorities from DB")
    @GetMapping(path = "authorities")
    public List<Authority> getAllAuthorities() {
        return authorityLogicService.getAllAuthorities();
    }

    // ---------- POST ----------

    /**
     * Null checks are done on DataAccess layer
     * */
    @Nullable
    @ApiOperation(value = "Inserts an authority into DB from provided authority")
    @PostMapping(path = "authority")
    public String postAuthority(@Nullable @RequestBody Authority authority) {
        return authorityLogicService.postAuthority(authority);
    }

    @Nullable
    @ApiOperation(value = "Inserts authorities into DB from provided authorities")
    @PostMapping(path = "authorities")
    public List<String> postAuthorities(@Nullable @RequestBody Authority[] authorities) {
        return (authorities == null || authorities.length == 0)
            ? null
            : authorityLogicService.postAuthorities(authorities);
    }

    // ---------- UPDATE ----------

    /**
     * Null checks are done on DataAccess layer
     * */
    @ApiOperation(value = "Updates a authority by UUID from provided authority")
    @PutMapping(path = "authority")
    public void updateAuthorityById(@Nullable @RequestBody Authority authority) {
        authorityLogicService.updateAuthorityById(authority);
    }

    @ApiOperation(value = "Updates authorities by UUIDs from provided authorities")
    @PutMapping(path = "authorities")
    public void updateAuthoritiesById(@Nullable @RequestBody Authority[] authorities) {
        if (authorities == null || authorities.length == 0) return;
        authorityLogicService.updateAuthoritiesById(authorities);
    }

    // ---------- DELETE ----------

    @Nullable
    @ApiOperation(value = "Deletes an authority by authority CUUID")
    @DeleteMapping("authority/{cuuid}")
    public Integer deleteAuthorityById(@Nullable @PathVariable("cuuid") CUUID cuuid) {
        UUID id = CUUID.toUUID(cuuid);
        if (id == null) return null;
        return authorityLogicService.deleteAuthorityById(id);
    }

    @Nullable
    @ApiOperation(value = "Deletes authorities by authority CUUIDs")
    @DeleteMapping("authorities/{cuuids}")
    public Integer deleteAuthoritiesById(@Nullable @PathVariable("cuuids") String cuuids) {
        UUID[] ids = CUUID.parseContinuousCUUIDString(cuuids);
        if (ids == null) return null;
        return authorityLogicService.deleteAuthoritiesById(ids);
    }
}