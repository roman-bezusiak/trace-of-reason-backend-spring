package com.tor.demo.api;

import com.tor.demo.model.Group;
import com.tor.demo.service.GroupLogicService;
import com.tor.demo.util.CUUID;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@Api(tags = "Group CRUD")
@RestController
@RequestMapping("api/")
class GroupController {
    private final GroupLogicService groupLogicService;

    @Autowired
    public GroupController(GroupLogicService groupLogicService) {
        this.groupLogicService = groupLogicService;
    }

    // ---------- GET ----------

    @Nullable
    @ApiOperation(value = "Returns a group found by group CUUID")
    @GetMapping(path = "group/{cuuid}")
    public Group getGroupById(@Nullable @PathVariable("cuuid") String cuuid) {
        UUID id = CUUID.parseSingleCUUIDString(cuuid);
        return id == null
            ? null
            : groupLogicService.getGroupById(id);
    }

    @Nullable
    @ApiOperation(value = "Returns groups found by group CUUIDs")
    @GetMapping(path = "groups/{cuuids}")
    public List<Group> getGroupsById(@Nullable @PathVariable("cuuids") String cuuids) {
        UUID[] ids = CUUID.parseContinuousCUUIDString(cuuids);
        return ids == null
            ? null
            : groupLogicService.getGroupsById(ids);
    }

    @Nullable
    @ApiOperation(value = "Returns groups found by group name or group alias")
    @GetMapping(path = "groups/name/{name}")
    public List<Group> getGroupsByName(@Nullable @PathVariable("name") String name) {
        return (name == null || name.isBlank())
            ? null
            : groupLogicService.getGroupsByName(name);
    }

    @Nullable
    @ApiOperation(value = "Returns all groups from DB")
    @GetMapping(path = "groups")
    public List<Group> getAllGroups() {
        return groupLogicService.getAllGroups();
    }

    // ---------- POST ----------

    /**
     * Null checks are done on DataAccess layer
     * */
    @Nullable
    @ApiOperation(value = "Inserts a group into DB from provided group")
    @PostMapping(path = "group")
    public String postGroup(@Nullable @RequestBody Group group) {
        return groupLogicService.postGroup(group);
    }

    @Nullable
    @ApiOperation(value = "Inserts groups into DB from provided groups")
    @PostMapping(path = "groups")
    public List<String> postGroups(@Nullable @RequestBody Group[] groups) {
        return (groups == null || groups.length == 0)
            ? null
            : groupLogicService.postGroups(groups);
    }

    // ---------- UPDATE ----------

    /**
     * Null checks are done on DataAccess layer
     * */
    @ApiOperation(value = "Updates a group by UUID from provided group")
    @PutMapping(path = "group")
    public void updateGroupById(@Nullable @RequestBody Group group) {
        groupLogicService.updateGroupById(group);
    }

    @ApiOperation(value = "Updates groups by UUIDs from provided groups")
    @PutMapping(path = "groups")
    public void updateGroupsById(@Nullable @RequestBody Group[] groups) {
        if (groups == null || groups.length == 0) return;
        groupLogicService.updateGroupsById(groups);
    }

    // ---------- DELETE ----------

    @Nullable
    @ApiOperation(value = "Deletes a group by group CUUID")
    @DeleteMapping("group/{cuuid}")
    public Integer deleteGroupById(@Nullable @PathVariable("cuuid") CUUID cuuid) {
        UUID id = CUUID.toUUID(cuuid);
        if (id == null) return null;
        return groupLogicService.deleteGroupById(id);
    }

    @Nullable
    @ApiOperation(value = "Deletes groups by group CUUIDs")
    @DeleteMapping("groups/{cuuids}")
    public Integer deleteGroupsById(@Nullable @PathVariable("cuuids") String cuuids) {
        UUID[] ids = CUUID.parseContinuousCUUIDString(cuuids);
        if (ids == null) return null;
        return groupLogicService.deleteGroupsById(ids);
    }
}