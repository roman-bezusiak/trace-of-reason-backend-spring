package com.tor.demo.service;

import com.tor.demo.dataaccess.GroupDataAccessService;
import com.tor.demo.dataaccess.NodeDataAccessService;
import com.tor.demo.model.Group;
import com.tor.demo.model.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Service
public class GroupLogicService {
    private final NodeDataAccessService nodeDAO;
    private final GroupDataAccessService groupDAO;

    @Autowired
    public GroupLogicService(
        NodeDataAccessService nodeDAO,
        GroupDataAccessService groupDAO
    ) {
        this.nodeDAO = nodeDAO;
        this.groupDAO = groupDAO;
    }

    // ---------- GET ----------

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public Group getGroupById(@NotNull UUID groupId) {
        return groupDAO.getGroupById(groupId);
    }

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public List<Group> getGroupsById(@NotNull UUID[] groupIds) {
        return groupDAO.getGroupsById(groupIds);
    }

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public List<Group> getGroupsByName(@NotNull String groupName) {
        return groupDAO.getGroupsByName(groupName);
    }

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public List<Group> getAllGroups(){
        return groupDAO.getAllGroups();
    }

    // ---------- POST ----------

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public String postGroup(@Nullable Group group) {
        return groupDAO.postGroup(group);
    }

    /**
     * STATUS: Reviewed
     * */
    @NotNull
    public List<String> postGroups(@NotNull Group[] groups) {
        return groupDAO.postGroups(groups);
    }

    // ---------- UPDATE ----------

    /**
     * STATUS: Reviewed
     * */
    public void updateGroupById(@Nullable Group group) {
        groupDAO.updateGroupById(group);
    }

    /**
     * STATUS: Reviewed
     * */
    public void updateGroupsById(@NotNull Group[] groups) {
        groupDAO.updateGroupsById(groups);
    }

    // ---------- DELETE ----------

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public Integer deleteGroupById(@NotNull UUID groupId) {
        deleteNodeLinksOfGroup(groupId);
        return groupDAO.deleteGroupById(groupId);
    }

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public Integer deleteGroupsById(@NotNull UUID[] groupIds) {
        for (int i = 0; i < groupIds.length; ++i) deleteNodeLinksOfGroup(groupIds[i]);
        return groupDAO.deleteGroupsById(groupIds);
    }

    /**
     * STATUS: Reviewed
     * */
    public void deleteNodeLinksOfGroup(@NotNull UUID id) {
        Node[] nodes = nodeDAO.getNodesByGroupId(id).toArray(Node[]::new);
        UUID[] idsBuffer;
        Node nodeBuffer = new Node();
        int lengthBuffer;

        // Updating affiliated nodes in node table
        for (int i = 0; i < nodes.length; ++i) {
            nodeBuffer.id = nodes[i].id;
            lengthBuffer = nodes[i].groupIds.length - 1;
            idsBuffer = new UUID[lengthBuffer];
            for (int j = 0; j < lengthBuffer; ++j) {
                if (!nodes[i].groupIds[j].equals(id)) {
                    idsBuffer[i] = nodes[i].groupIds[j];
                }
            }
            nodeBuffer.groupIds = idsBuffer;
            nodes[i] = nodeBuffer;
        }
        nodeDAO.updateNodesById(nodes);
    }
}

