package com.tor.demo.service;

import com.tor.demo.dataaccess.NodeDataAccessService;
import com.tor.demo.model.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Service
public class NodeLogicService {
    private final NodeDataAccessService nodeDAO;

    @Autowired
    public NodeLogicService(NodeDataAccessService nodeDAO) {
        this.nodeDAO = nodeDAO;
    }

    // ---------- GET ----------

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public Node getNodeById(@NotNull UUID nodeId) {
        return nodeDAO.getNodeById(nodeId);
    }

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public List<Node> getNodesById(@NotNull UUID[] nodeIds) {
        return nodeDAO.getNodesById(nodeIds);
    }

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public List<Node> getNodesByGroupId(@NotNull UUID groupId) {
        return nodeDAO.getNodesByGroupId(groupId);
    }

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public List<Node> getNodesByAuthorityId(@NotNull UUID authorityId) {
        return nodeDAO.getNodesByAuthorityId(authorityId);
    }

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public List<Node> getNodesByName(@NotNull String nodeName) {
        return nodeDAO.getNodesByName(nodeName);
    }

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public List<Node> getAllNodes(){
        return nodeDAO.getAllNodes();
    }

    // ---------- CREATE ----------

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public String postNode(@Nullable Node node) {
        return nodeDAO.postNode(node);
    }

    /**
     * STATUS: Reviewed
     * */
    @NotNull
    public List<String> postNodes(@NotNull Node[] nodes) {
        return nodeDAO.postNodes(nodes);
    }

    // ---------- UPDATE ----------

    /**
     * STATUS: Reviewed
     * */
    public void updateNodeById(@Nullable Node node) {
        nodeDAO.updateNodeById(node);
    }

    /**
     * STATUS: Reviewed
     * */
    public void updateNodesById(@NotNull Node[] nodes) {
        nodeDAO.updateNodesById(nodes);
    }

    // ---------- DELETE ----------

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public Integer deleteNodeById(@NotNull UUID nodeId) {
        deleteRelationLinksOfNode(nodeId);
        return nodeDAO.deleteNodeById(nodeId);
    }

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public Integer deleteNodesById(@NotNull UUID[] nodeIds) {
        for (int i = 0; i < nodeIds.length; ++i) deleteRelationLinksOfNode(nodeIds[i]);
        return nodeDAO.deleteNodesById(nodeIds);
    }

    /**
     * STATUS: Reviewed
     * */
    public void deleteRelationLinksOfNode(@NotNull UUID id) {
        // Check for node existence
        Node node = nodeDAO.getNodeById(id);
        if (node == null) return;

        UUID[] parentIds = node.parentIds;
        UUID[] childIds = node.childIds;
        Node[] parentNodes = nodeDAO.getNodesById(parentIds).toArray(Node[]::new);
        Node[] childNodes = nodeDAO.getNodesById(childIds).toArray(Node[]::new);
        UUID[] idsBuffer;
        Node nodeBuffer = new Node();
        int lengthBuffer;

        // Updating parent nodes array
        for (int i = 0; i < parentNodes.length; ++i) {
            nodeBuffer.id = parentIds[i];
            lengthBuffer = parentNodes[i].childIds.length - 1;
            idsBuffer = new UUID[lengthBuffer];
            for (int j = 0; j < lengthBuffer; ++j) {
                if (!parentNodes[i].childIds[j].equals(id)) {
                    idsBuffer[i] = parentNodes[i].childIds[j];
                }
            }
            nodeBuffer.childIds = idsBuffer;
            parentNodes[i] = nodeBuffer;
        }

        nodeBuffer.childIds = null;

        // Updating child nodes array
        for (int i = 0; i < childNodes.length; ++i) {
            nodeBuffer.id = childIds[i];
            lengthBuffer = childNodes[i].parentIds.length - 1;
            idsBuffer = new UUID[lengthBuffer];
            for (int j = 0; j < lengthBuffer; ++j) {
                if (!childNodes[i].parentIds[j].equals(id)) {
                    idsBuffer[i] = childNodes[i].parentIds[j];
                }
            }
            nodeBuffer.parentIds = idsBuffer;
            childNodes[i] = nodeBuffer;
        }

        nodeDAO.updateNodesById(parentNodes); // Updating parent nodes in DB
        nodeDAO.updateNodesById(childNodes); // Updating child nodes in DB
    }
}
