package com.tor.demo.service;

import com.tor.demo.dataaccess.AuthorityDataAccessService;
import com.tor.demo.dataaccess.NodeDataAccessService;
import com.tor.demo.model.Authority;
import com.tor.demo.model.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Service
public class AuthorityLogicService {
    private final NodeDataAccessService nodeDAO;
    private final AuthorityDataAccessService authorityDAO;

    @Autowired
    public AuthorityLogicService(
        NodeDataAccessService nodeDAO,
        AuthorityDataAccessService authorityDAO
    ) {
        this.nodeDAO = nodeDAO;
        this.authorityDAO = authorityDAO;
    }

    // ---------- GET ----------

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public Authority getAuthorityById(@NotNull UUID authorityId) {
        return authorityDAO.getAuthorityById(authorityId);
    }

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public List<Authority> getAuthoritiesById(@NotNull UUID[] authorityIds) {
        return authorityDAO.getAuthoritiesById(authorityIds);
    }

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public List<Authority> getAuthoritiesByName(@NotNull String authorityName) {
        return authorityDAO.getAuthoritiesByName(authorityName);
    }

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public List<Authority> getAllAuthorities(){
        return authorityDAO.getAllAuthorities();
    }

    // ---------- POST ----------

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public String postAuthority(@Nullable Authority authority) {
        return authorityDAO.postAuthority(authority);
    }

    /**
     * STATUS: Reviewed
     * */
    @NotNull
    public List<String> postAuthorities(@NotNull Authority[] authorities) {
        return authorityDAO.postAuthorities(authorities);
    }

    // ---------- UPDATE ----------

    /**
     * STATUS: Reviewed
     * */
    public void updateAuthorityById(@Nullable Authority authority) {
        authorityDAO.updateAuthorityById(authority);
    }

    /**
     * STATUS: Reviewed
     * */
    public void updateAuthoritiesById(@NotNull Authority[] authorities) {
        authorityDAO.updateAuthoritiesById(authorities);
    }

    // ---------- DELETE ----------

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public Integer deleteAuthorityById(@NotNull UUID authorityId) {
        deleteNodeLinksOfAuthority(authorityId);
        return authorityDAO.deleteAuthorityById(authorityId);
    }

    /**
     * STATUS: Reviewed
     * */
    @Nullable
    public Integer deleteAuthoritiesById(@NotNull UUID[] authorityIds) {
        for (int i = 0; i < authorityIds.length; ++i) deleteNodeLinksOfAuthority(authorityIds[i]);
        return authorityDAO.deleteAuthoritiesById(authorityIds);
    }

    /**
     * STATUS: Reviewed
     * */
    public void deleteNodeLinksOfAuthority(@NotNull UUID id) {
        Node[] nodes = nodeDAO.getNodesByAuthorityId(id).toArray(Node[]::new);
        UUID[] idsBuffer;
        Node nodeBuffer = new Node();
        int lengthBuffer;

        // Updating affiliated nodes in node table
        for (int i = 0; i < nodes.length; ++i) {
            nodeBuffer.id = nodes[i].id;
            lengthBuffer = nodes[i].authorityIds.length - 1;
            idsBuffer = new UUID[lengthBuffer];
            for (int j = 0; j < lengthBuffer; ++j) {
                if (!nodes[i].authorityIds[j].equals(id)) {
                    idsBuffer[i] = nodes[i].authorityIds[j];
                }
            }
            nodeBuffer.authorityIds = idsBuffer;
            nodes[i] = nodeBuffer;
        }

        nodeDAO.updateNodesById(nodes);
    }
}

