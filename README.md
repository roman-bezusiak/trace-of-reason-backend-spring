# Trace of reason

![TOR logo](./img/logo1000.png)

## 📋 Description

This project is aimed to provide tooling for data visualization and linkage.
It is currently available [online](https://trace-of-reason-backend-spring.herokuapp.com/swagger-ui.html) (User: `admin`, Pass: `test`)

## 📥 Installation instructions

1. Clone the repo (`git clone https://gitlab.com/roman-bezusiak/trace-of-reason-backend-spring.git`)
2. Unzip
3. Move into the root directory (`cd trace-of-reason-frontend-react`)

## 🛠 Configuration instructions

1. Go to `src/main/resources/application.properties`
2. Change `JDBC_DATABASE_URL` to your local DB URL

## ⚙️ Operating instructions

1. Run PostgreSQL database
2. Run Maven pipeline (`mvn clean install`)
3. Run Heroku locally (`heroku local web`)
